Instructions:
Create a Gitlab account
Create a repository on Gitlab as name-surname-module-2
Create 3 .dart files for the exercise. All code outputs are printed on the console.


Assessment:
All the code must be submitted through a Gitlab repository link. The code must be on three files, each with this code:

Write a basic program that stores and then prints the following data: Your name, favorite app, and city;
Create an array to store all the winning apps of the MTN Business App of the Year Awards since 2012; a) Sort and print the apps by name;  b) Print the winning app of 2017 and the winning app of 2018.; c) the Print total number of apps from the array.
Create a class and a) then use an object to print the name of the app, sector/category, developer, and the year it won MTN Business App of the Year Awards. b) Create a function inside the class, transform the app name to all capital letters and then print the output.
